package com.Ridit.java.Singleton;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Singleton object1 = Singleton.getInstance();
		Singleton object2 = Singleton.getInstance();
		if (object1.hashCode() == object2.hashCode()) {
			System.out.println("this is Singleton");
		}

	}

}
